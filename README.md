# Archinstall auto

Just run this command
---
```bash
bash <(curl -s https://gitlab.com/KateFreesex/open/-/raw/main/archinstall/archinstall.sh)  
```

# For change encryption boot pass
```bash
# Test pass
cryptsetup --test-passphrase -v open /dev/sda2

# Show your lots
cryptsetup luksDump /dev/sda2

# Add new lot
cryptsetup luksAddKey /dev/sda2

# Delete firts lot
cryptsetup luksKillSlot /dev/sda2 0
```

# For change your user pass
```bash
passwd
```
